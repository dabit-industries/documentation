# 3D SLAM with RGB-D Sensors
An RGB-D sensor, like the Orbbec Astra or Microsoft Kinect, houses a color camera (RGB) along with a depth camera (D).
Using the depth sensor, we can map the color image on top of the depth image to get a 3D color image.

## RTABMAP
RTABMAP (Real-Time Appearance-Based Mapping) is a RGB-D Graph-Based SLAM approach based on an incremental appearance-based global Bayesian loop closure detector.

RTABMAP combines gmapping with 3D SLAM in order to do autonomous navigation (in the next tutorials) while 3D mapping.
Along with 3D point cloud data from the depth sensor, RTABMAP also collects 2D and 3D laser scan data.

### Installation
These instalation steps are to be done on BOTH the Turtlebot and Master computers.

1. Connect to a Wi-Fi network that has internet access
2. Open a new terminal
    1. `sudo apt-get install ros-kinetic-rtabmap-ros -y`
3. Connect back to your Turtlebot Wi-Fi network.

### Starting RTABMAP on the Turtlebot
In order to start and configure RTABMAP properly, we need to create our own launch file on the Turtlebot.

1. Create a workspace folder on the Turtlebot computer:
    1. On the Turtlebot Computer, open a new terminal
        * `mkdir ~/workspace`
        * `cd ~/workspace`

2. Create a new launch file to start RTABMAP on the Turtlebot with default configurations:
    1. On the Turtlebot computer, open a new terminal
        * `cd ~/workspace`
        * save [rtabmap.launch](scripts/rtabmap.launch) to workspace
 

3. On Turtlebot, start minimal.launch
    1. `roslaunch turtlebot_bringup minimal.launch`

4. On Turtlebot, start the rtabmap.launch 
    1. `cd ~/workspace`
    2. `roslaunch rtabmap.launch database_path:=~/workspace/rtabmap.db`
        * the rtabmap.launch automatically launches 3dsensor.launch
        * the argument `database_path:=` is the location of the database file
            * if no database exists, rtabmap will create a new one

### Visualizing RTABMAP data

1. On the master computer, start RVIZ
    1. `roslaunch rtabmap_ros demo_turtlebot_rviz.launch`

2. On the master computer, start keyboard teleop
    1. `roslaunch turtlebot_teleop keyboard_teleop.launch`

3. Drive the Turtlebot around your map using the keyboard_teleop, and visualize the collected data in RVIZ.
    * In order to collect data fully, you will need to stop the turtlebot periodically and slowly turn the turtlebot in circles  
    ![](assets/07-rtabmap_circle.gif)

    * You can bring up an image view in RVIZ by adding a new display:  
      * Under "Displays" click the "Add" button
      * Click the "By Topic" tab
      * Select the drop-down for `/camera/rgb/image_raw`
      * Click on "Image"
      * Click "Ok"
    ![](assets/07-rtabmap_image_view.gif)

    * While you drive the Turtlebot around, it will begin to generate a map  
    ![](assets/07-rtabmap_map.gif)
      * The white in the map is known empty space
      * The dark borders around the white are known obstacles such as walls and other objects
      * The grey tiles surrounding the map is unknown space
      * The 3d points that are verified using closed-loop detection will be shown in RVIZ

![](assets/07-rtabmap_collected.gif)

### Viewing the RTABMAP data
RTABMAP provides their own tool to visualize and manipulate data.  
With the `rtabmap-databaseViewer` tool, you can:
  - Open rtabmap databases
  - View rtabmap 2d and 3d data
  - Detect more loop closures
  - Refine detected loop closures
  - Generate a 3D map file (.ply, .pcd)
  - Generate 2D-3D laser scans (.ply, .pcd)

1. Copy the database from the Turtlebot to the Master Computer
    1. On the Master Computer, open a new terminal:
        * `cd ~/workspace`
        * `scp turtlebot@IP_OF_TURTLEBOT:~/workspace/rtabmap.db .`
          * The remote copy operation may take some time depending on the database size

2. Use `rtabmap-databaseViewer` to view the rtabmap data
    1. On the master computer, in a terminal:
        * `cd ~/workspace`
        * `rtabmap-databaseViewer rtabmap.db`
    2. If a prompt comes up to Update Parameters
        * Press "Yes"  
        ![](assets/07-rtabmap_database_update_parameters.png)
    3. The default display in the database viewer shows the loop closure detection  
    ![](assets/07-rtabmap_loop_closure_display.png)
    4. To view the 3D map:
        * Press "Edit" on the top bar
        * Press "View 3D Map"
        * Select "4" and hit "OK"
        * Type "8.0" and hit "OK"
        * Scroll out and move around the 3D point coud map  
        ![](assets/07-rtabmap_3d_view.gif)

### Tuning RTABMAP Parameters
Coming soon, we are working out the best parameters to use with this Turtlebot setup

## RGBDSLAM
RGBDSLAM is a RGB-D loop-closure graph-based SLAM approach using visual odometry and custom implementations of opencv methods  

!!! warning "WIP"
    This section is a Work In Progress and is considered incomplete.

### Prequisites
This section **requires** the *catkin_ws* to be initialized and the *turtlebot_dabit* package created.

[Please click here to learn how to initialize the catkin workspace](08-Catkin_Workspace.md)

### Installation
This process has to be done on __both__ _master_ and _turtlebot_ computers.

Download rgbdslam_v2 and configure dependencies

1. CD to catkin_ws, source the environment setup, and download rgbdslam_v2
    * `cd ~/catkin_ws/src`
    * `git clone -b kinetic https://github.com/felixendres/rgbdslam_v2 rgbdslam`
2. Remove any conflicting libraries
    * `sudo apt remove ros-kinetic-libg2o libqglviewer-dev`
3. Install build dependencies
    * `sudo apt install cmake libeigen3-dev libsuitesparse-dev`
4. Build and install a custom verison of g2o
    * `cd ~/catkin_ws/src`
    * `git clone -b c++03 https://github.com/felixendres/g2o.git`
    * `mkdir g2o/build`
    * `cd g2o/build`
    * `cmake .. -DCMAKE_INSTALL_PREFIX=../install -DG2O_BUILD_EXAMPLES=OFF`
    * `nice make -j4 install`
      * If your build fails, try the `nice make -j4 install` command again
5. Install the library dependencies of rgbdslam_v2 using rosdep
    * `source ~/catkin_ws/devel/setup.sh`
    * `rosdep update`
    * `rosdep install rgbdslam`
6. Build rgbdslam
    * `export G2O_DIR=~/catkin_ws/src/g2o/install`
    * `cd ~/catkin_ws`
    * `catkin_make`

### Starting RGBDSLAM on the Turtlebot
On the _Turtlebot_ laptop:

1. Open a new terminal
    1. `source ~/catkin_ws/devel/setup.sh`
    2. `export G2O_DIR=~/catkin_ws/src/g2o`
    3. `roslaunch rgbdslam headless.launch`

### Visualizing RGBDSLAM data
On the _master_ laptop, start the rgbdslam interface:

1. Open a new terminal
    1. `source ~/catkin_ws/devel/setup.sh`
    2. `rosrun rgbdslam rgbdslam`
2. Use the GUI to view the data from the rgbdslam node

### Tuning RGBDSLAM Parameters
Coming soon, we are working out the best parameters to use with this Turtlebot setup

## ORB SLAM 2

!!! warning "WIP"
    This section is a Work In Progress and is considered incomplete.

### Installation
Download orb_slam2 and configure dependencies

1. CD to workspace and download orb_slam2
    * `cd ~/workspace``
    * `git clone https://github.com/raulmur/ORB_SLAM2``
2. Make a temporary directory in the workspace
    * `mkdir ~/workspace/tmp`
3. Build and Install Intel LibRealSense
    * `cd ~/workspace/tmp`
    * `git clone https://github.com/IntelRealSense/librealsense`
    * `cd librealsense`
    * `mkdir build`
    * `cd build`
    * `cmake ..`
    * `make`
    * `sudo make install`
4. Build and Install Pangolin
    * `cd ~/workspace/tmp`
    * `git clone https://github.com/stevenlovegrove/Pangolin.git`
    * `git checkout v0.5`
    * `sudo apt install libglew-dev`
    * `cd Pangolin`
    * `mkdir build`
    * `cd build`
    * `cmake ..`
    * `make`
    * `sudo make install`
5. Build ORB_SLAM2 base libraries
    * `cd ~/workspace/ORB_SLAM2`
    * `chmod +x build.sh`
    * `./build.sh`
6. Build ORB_SLAM2 for ROS
    * `export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:~/workspace/ORB_SLAM2/Examples/ROS`
    * `chmod +x build_ros.sh`
    * `./build_ros.sh`
7. Run ORB_SLAM2 RGBD Example
    * `rosrun ORB_SLAM2 RGBD ~/workspace/ORB_SLAM2/Vocabulary/ORBvoc.txt ~/workspace/ORB_SLAM2/Examples/RGB-D/TUM1.yaml`

### Starting ORB_SLAM2 on the Turtlebot
No support yet

### Visualizing ORB_SLAM data
No support yet

### Viewing ORB_SLAM data
No support yet

### Tuning ORB_SLAM parameters
No support yet
