# Viewing Robots with Blender + Phobos

## Viewing a xacro/urdf
```
xacro --inorder /path/to/xacro > robot.urdf
gz sdf -p robot.urdf > robot.sdf
```
