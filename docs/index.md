# Dabit Industries Documentation

---

These pages are a collection of projects, product, snippets,
and other adventures, services, and offerings from Dabit
Industires.

<!-- Notes on dabit -->
<!-- Supported platforms -->
<!-- System images -->
<!-- Projects -->
<!-- ? -->

<center><h2>**Robots**</h2></center><br/>
<table class="arm-selection"><tbody><tr>
<td valign="top" align="center">
[![Turtlebot 2](/assets/img/TB2.jpg) **Turtlebot 2**](robots/turtlebot2)</td>
<td valign="top" align="center">
[![Turtlebot 3](/assets/img/TB3_Burger.jpg) **Turtlebot 3**](robots/turtlebot3)</td>
</tr><tr>
<td valign="top" align="center">
[![Open Manipulator](/assets/img/OpenManipulator.jpg) **Open Manipulator**](robots/openmanipulator)</td>
<td valign="top" align="center">
[![Interbotix X-Series](/assets/img/reac200_1.jpg) **Interbotix X-Series**](robots/x-series)</td>
</tr></tbody></table>

<center><h2>**Projects**</h2></center><br/>
<table class="arm-selection"><!--{: style="tr td { max-width: 400px; }"}--> <tbody><tr>
<td valign="top" align="center">
[![Turtlebot 2 + RX200](/assets/img/TB2_RX200.jpg){: style="height:200px;width:200px;"} **Turtlebot 2 + ReactorX 200**](projects/tb2_rx200)</td>
<!--td valign="top" align="center">
[![Turtlebot 2 + Quanergy M8](/assets/img/TB2_M8.jpg) **Turtlebot 2 + Quanergy M8 LiDAR**](projects/tb2_m8)</td-->
</tr><tr></table>

