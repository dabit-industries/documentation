# OpenCM9.04 Controller
Robotis OpenCM9.04 STM32 Micro Controller

-----

The [OpenCM9.04][opencm] is an open-source controller by ROBOTIS that uses a 32bit ARM Cortex-M3 STM32F103CB.

## X-Series Usage
At this time, the [OpenCM9.04][opencm] only serve as a serial passthrough using the [Dynamixel Passthrough][usb_to_dxl] firmware.  
The current firmware has issues and limitations, as marked below.  

### Dynamixel Passthrough
In Passthrough mode, the OpenCM will transfer packets between the USB serial interface and the Dynamixel serial interface.  
This is done by retrieving available data from each interface and writing it to the other.
Dynamixel USB Passthrough may be referred to as "usb_to_dxl".

At this time, **ONLY the OpenCM9.04C variant is supported**. The Trossen x-series arms ship with the OpenCM9.04C variant by default.

#### Installation
To install Dynamixel Passthrough, please use the [DMXL](../dmxl/init.md) utility to manage firmware upgrades and controller setup.  

If you would like to install without using [DMXL](https://git.sr.ht/~lucidone/dmxl), refer to the README in the [firmware repository][usb_to_dxl].

#### Known Limitations
There are some limitations in the current [Dynamixel Passthrough][usb_to_dxl] implementation.
- Due to packet loss (and potential race conditions), the OpenCM9.04c may drop out from time to time.
  - This may make the servos jittery, and may contribute to slower response times. At worst, it could drop connection between ROS and the Dynamixels.

## Resources
- [OpenCM9.04C Product Page](#TODO)
- [OpenCM9.04 Manual][opencm]
- [Robotis OpenCM9.04 Github Repository](https://github.com/ROBOTIS-GIT/OpenCM9.04)

[opencm]: http://emanual.robotis.com/docs/en/parts/controller/opencm904/
[usb_to_dxl]: https://gitlab.com/dabit-industries/interbotix_x-series/dynamixelsdk-usb_to_dxl
