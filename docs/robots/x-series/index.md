# Overview
The Interbotix X-Series Robot Arms are a line of robotic arms built around the high performance Robotis DYNAMIXEL X-Series smart servos.

---

<iframe class="youtube-embed" src="https://www.youtube.com/embed/SVpKO5GwwAg?playlist=SVpKO5GwwAg&controls=0&mute=1&color=white&rel=0&autoplay=1&loop=1&modestbranding=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Introduction
The new Interbotix X-Series Robot Arms are built around the high performance DYNAMIXEL X-Series smart servos from Robotis.
The X-Series actuators offer higher torque, more efficient heat dissipation and better durability all at a smaller form factor over previous DYNAMIXEL servos.
There are a total of 8 arms in the new line-up offering many different configurations of length and payload with reach up to 750mm and working payload up to 750g, 
each of the arms comes assembled and tested out of the box. The arms (Pincher X 150 model or higher) come standard with an upgraded slewing bearing base, 
a wider customizable parallel gripper configuration and covered semi-transparent electronics shield that offers easy accessibility to ports while keeping 
the control board free from debris and safe from impact.

!!! WARNING "ROS Packages"
    Currently, only the ReactorX 150, WidowX 200, and WidowX 250 have ROS support.

## QuickStart
This QuickStart guide covers the system/software quick start.

<iframe class="youtube-embed" src="https://www.youtube.com/embed/Ke6QIpptWzQ?controls=1&mute=1&color=white&rel=0&autoplay=0&modestbranding=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Getting started with the software environment.  
The best way to get started using the X-Series arms is to download and run the [Ubuntu 16.04 Interbotix ISO image][iso].  
[Click here](setup.md#iso) for more information on the iso.  

If you prefer to install the packages on your own image, check out [installing the x-series packages](setup.md#workspace) page.

The rest of this section refers to the ISO usage.

[iso]: setup.md#iso-download

### Booting and Using the ISO
After following the [USB flashing instructions](setup.md#iso-flashing) and [booting instructions](setup.md#iso-booting),
you should be able to boot into an Ubuntu LiveCD from a modern laptop or desktop.

### Use DMXL to Set Up Your Arm
[dmxl][dmxl] is a software tool that makes system and servo configuration easy for Dynamixel-based robots.

To start, we must initialize the dmxl config and system. This is automatically taken care of in newer ISO builds.  

```bash
dmxl init config
dmxl init system
```

#### Upload Controller Firmware
From the factory, the X-Series arms are not loaded with firmware.  
[dmxl][dmxl] simplifies firmware loading to 1 step:

```bash
dmxl init controller
```

Running this command, [dmxl][dmxl] will automatically identify any of the supported controllers, and upload the [USB passthrough firmware](https://gitlab.com/dabit-industries/interbotix/x-series/dynamixelsdk-usb_to_dxl).

#### Scan and Test Servos
Now scan the servos connected to your arm:

```bash
dmxl run scan
```

[dmxl][dmxl] should report any servo ids found, typically with ids between 1-10.  
If the servos for your arm are found, blink the servos to verify their order:  

```bash
dmxl run blink
```

If successful, each servo should enable and disable their LED sequentially.

#### Remap Servo IDs
Currently, the servo IDs need to be remapped to further configure and control your arm.  
Configurations vary between servos, be sure to replace `ARM_NAME=wx200` in the following example with the arm you have.  

```bash
export ARM_NAME=wx200
dmxl run remap --profile $(rospack find interbotix_${ARM_NAME}_controller)/config/dmxl.toml ros
```

#### Profile Your Arm
Now that the servos are in the proper map, apply the recommended configuration to the servos:  

```bash
export ARM_NAME=wx200
dmxl run tune --profile $(rospack find interbotix_${ARM_NAME}_controller)/config/dmxl.toml
```

### Using the Launcher 
For convinience, a [GUI launcher utility](operation/launcher.md) is provided with the ROS packages.  
This launcher allows access out-of-the-box to the most-used sotware for the arms including MoveIt!, Gazebo, and RVIZ.

On the desktop, there is an  Interbotix X icon titled "X-Series Launcher". Double-click the icon to open the launcher.  
![](/assets/img/Interbotix_Logo_bg_x32.png)

Alternatively, use the following command to run the launcher from a terminal:

```bash
rosrun interbotix_x-series_launcher launcher.py
```

[dmxl]: https://git.sr.ht/~lucidone/dmxl

## Learning More About Your Arm

<center>**Choose your X-Series model below!**</center><br/>
<table class="arm-selection"><tbody><tr>
<td valign="top" align="center">
[![PincherX 100](/assets/img/pin100_1.jpg) **PincherX 100**](pincherx_100.md)</td>
<td valign="top" align="center">
[![PincherX 150](/assets/img/pin150_1.jpg) **PincherX 150**](pincherx_150.md)</td>
</tr><tr>
<td valign="top" align="center">
[![ReactorX 150](/assets/img/reac150_1.jpg) **ReactorX 150**](reactorx_150.md)</td>
<td valign="top" align="center">
[![ReactorX 200](/assets/img/reac200_1.jpg) **ReactorX 200**](reactorx_200.md)</td>
</tr></tbody><tbody><tr>
<td valign="top" align="center">
[![WidowX 200](/assets/img/wid200_1.jpg) **WidowX 200**](widowx_200.md)</td>
<td valign="top" align="center">
[![WidowX 250](/assets/img/wid250_1.jpg) **WidowX 250**](widowx_250.md)</td>
</tr><tr>
<td valign="top" align="center">
[![ViperX 250](/assets/img/vip250_1.jpg) **ViperX 250**](viperx_250.md)</td>
<td valign="top" align="center">
[![ViperX 300](/assets/img/vip300_1.jpg) **ViperX 300**](viperx_300.md)</td>
</tr></tbody></table>

