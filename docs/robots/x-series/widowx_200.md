# WidowX 200 Robot Arm

!!! warning "WIP"
    This page is a Work In Progress and is considered incomplete.

The WidowX 200 Robot Arm belongs to a new family of arms from Interbotix featuring the DYNAMIXEL
X-Series Smart Servo Motors. See more information about the X-Series [here](../x-series)

## Specification

### Dynamixel Servo Layout

<div class="joint_table_image">
<img src="/assets/img/wx200_joints.jpg"/>
</div>

| Joint        | Servo ID<sup>1</sup> | Min Position Limit<sup>2</sup> | Max Position Limit<sup>2</sup> |
| :---:        | :---:                | :---:                          | :---:                          |
| **Base**     | 1                    | 0 DEG (0)                      | 360 DEG (4095)                 |
| **Shoulder** | 2 + 202              | 80 DEG (910)                   | 290 DEG (3314)                 |
| **Elbow**    | 3                    | 87 DEG (990)                   | 280 DEG (3185)                 |
| **Forearm**  | 4                    | 55 DEG (625)                   | 285 DEG (3242)                 |
| **Wrist**    | 5                    | 0 DEG (0)                      | 360 DEG (4095)                 |
| **Gripper**  | 6                    | 123 DEG (1400)                 | 255 DEG (2901)                 |

<div class="footnote"><ol>
1. The servo ID that drives the joint. Additional `+` servos are shadow servos that offer additional torque.<br />
2. Position limits are denoted by Degrees first followed by the raw integer value for the Dynamixel Servo. <br />
&nbsp;&nbsp;&nbsp;&nbsp;NOTE: These values are *not* the absolute limits, and may be offset 1-2 degrees for safety. MoveIt! uses absolute limits.<br />
</ol></div>

## Firmware Programming
Please see [Firmware Upload](../setup/#firmware)

## Software Overview and Setup

### Packages
Packages related to the WidowX 200 are located at https://gitlab.com/dabit-industries/interbotix_x-series/ros_x-series_wx200  

| Package                          | Description                                                                      |
| -------                          | -----------                                                                      |
| **interbotix_wx200**             | Metapackage                                                                      |
| **interbotix_wx200_bringup**     | Launch files and configurations related to starting various aspects of the wx200 |
| **interbotix_wx200_controller**  | Controller configurations for properly controlling the Dynamixels                |
| **interbotix_wx200_description** | The robot description, meshes, and files related to visualization                |
| **interbotix_wx200_gazebo**      | Relative configurations to run Gazebo Sim                                        |
| **interbotix_wx200_moveit**      | Relative configurations and launch files to use MoveIt! and related packages     |

## ROS Launch

### Full Bringup
`full.launch` is able to start the entire x-series stack for the WidowX-200 with minimal arguments.  
```console
roslaunch interbotix_wx200_bringup full.launch teleop:=false simulate:=false use_platform:=false use_controller:=true use_platform:=true use_moveit:=true publish_joint_states:=true gui_joint_state:=false --screen dynamixel_usb_port:=/dev/ttyACM0
```

#### Arguments

#### Launches
- [minimal.launch]

### Minimal Bringup

#### Arguments

### Simulation (Gazebo)

## Testing the Latest
- Follow the setup detailed in [Common Workspace](../usage/common_ws.md)  
- Clone the latest [ros_x-series_wx200](https://gitlab.com/dabit-industries/interbotix_x-series/ros_x-series_wx200) repository:
    - ```
      cd ~/catkin_ws/src
      git clone git@gitlab.com:dabit-industries/interbotix_x-series/ros_x-series_wx200
      ```
- Build your workspace
  - ```console
    cd ~/catkin_ws
    catkin_make
    ```
- Connect your arm, follow the [Common Arm Setup](../usage/common_arm_setup.md), and launch!
  - ```console
    source ~/catkin_ws/devel/setup.bash
    roslaunch interbotix_x-series_wx200 full.launch
    ```

## Resources
- [Trossen Robotics WidowX 200 Product Page](https://www.trossenrobotics.com/widowx-200-robot-arm.aspx)
- TODO: CAD
- TODO: Dmxl
- TODO: ?
