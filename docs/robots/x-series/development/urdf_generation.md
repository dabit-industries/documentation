# URDF Creation

## Manual Creation
If you're looking to customize your X-Series kit, read up on the ROS [urdf](https://wiki.ros.org/urdf) and [xacro](http://wiki.ros.org/xacro) documentation.

### Custom End Effector
TODO

## Semi-automated generation
!!! This page is under construction! Proceed at your own risk.

## OnShape
TODO

### Creating an Assembly

### Assigning joint dofs

### Adding Mass

### OnShape to URDF
Currently, we use [Onshape-to-robot](https://github.com/Rhoban/onshape-to-robot) from [Rhoban](http://rhoban.com/) to generate a URDF from an OnShape assembly.

## FreeCAD

### FreeCAD AppImage
[FreeCAD AppImage Download](#)

A FreeCAD AppImage which includes hte necessary dependencies to run under any Debian/Ubuntu system.

### FreeCAD Conda Environment
Use [Conda](#) to run FreeCAD built with Python 3.7.

```bash
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda
export PATH="$HOME/miniconda/bin:$PATH"

hash -r
conda config --set always_yes yes --set changeps1 no
conda update -q conda

conda create \
    -p FreeCAD-Conda/usr \
    gxx_linux-64 freecad calculix blas=*=openblas gitpython\
    --copy \
    --no-default-packages \
    -c freecad/label/dev \
    -c conda-forge/label/main \
    -y

source activate FreeCAD-Conda/usr
FreeCAD
```

## Blender
!!! TODO

Use [Blender3D](https://www.blender.org/) and [Phobos](https://github.com/dfki-ric/phobos) to create a robot description model with kinematics, collision geometry, inertia information, joint states and limits, and sensors.  
Ideally import an assembly from FreeCAD or OnShape.
