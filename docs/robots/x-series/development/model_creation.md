# Model Creation


-----

Import into FreeCAD appimage to fix step surfaces before importing into onshape
  import into freecad
  verify parts are whole (no individual surfaces)
  select assembly
  export > .step

import into onshape
  create > import > .step > import to a single document
  workspace units mm
  cylindrical joints, child -> parent
  
  get distances of joints
    make sure no other objects are selected
    select child joint first, then parent joint, look at bottom right of onshape window, record distance
    repeat for each joint
