# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
The next release is a complete revamp on the documentation.  
Stay tuned!

## [0.0.0] - 2019-05-14
Initial Release


[Unreleased](#)
[0.0.0](#)
