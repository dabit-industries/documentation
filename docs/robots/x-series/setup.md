# X-Series Robot Arm Setup

## ISO
The ISO is a pre-packaged Ubuntu-based system that can either be used as a LiveCD or installed to a system.

### ISO Download
Currently, only a development ISO image is being built at this time.  
Once [Ubuntu Packages][package-issue] are available, a stable ISO image will be available for download.

#### Development ISO
The latest iso version is: `pre13`  
!!! INFO "Download Ubuntu 16.04 + ROS Kinetic, Developnment pre13"
    **Download**: http://dev.dabitindustries.com/interbotix-ubuntu-16.04.6-ros-kinetic-amd64-pre13.iso  
    **sha256sum**: a5069e8752def0c1f479aa8370173d19b998de9d6e3bc66292ff3583567c8632  
    **Size**: 2.7G  

    **Included in this ISO**:  
    - Ubuntu 16.04 + ROS Kinetic  
    - X-Series ROS Packages + Related Packages  
    - [dmxl][dmxl]  
    - [Dynamixel Wizard 2.0](http://www.robotis.us/dynamixel-wizard-2-0/)  

### ISO Flashing
Please follow one of the following tutorials for your platform:  
- [Create a bootable USB stick on Windows](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows)  
- [Create a bootable USB stick on Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu)  
- [Create a bootable USB stick on Mac OS](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-macos)  

### ISO Booting
Booting USBs vary between platforms.  
This section will be filled out to include generic information relating to most laptop/desktop computers.

### ISO Installing
The ISO can be installed to the computer it is running on in the same fasion that the standard Ubuntu ISO can be installed.  
Simply run the Install program from boot or within the LiveCD.

## Manual
Manual installation is covered by [vcstool][vcstool] and [apt][apt].  

### Using VCS Tool
```bash
# Create the catkin workspace directory
mkdir ~/interbotix_ws
cd ~/interbotis_ws

# Download the rosinstall file
wget -O .rosinstall https://gitlab.com/dabit-industries/interbotix/interbotix-env-kinetic/raw/master/root/usr/share/interbotix/rosinstall

# Use vcstool to pull down the repositories
vcs import --input .rosinstall src

# Build the workspace
catkin_make

# Source the workspace
source ~/interbotix_ws/devel/setup.bash
```

<div class="footnote"><ol>
1. This process is automated with the following script: https://gitlab.com/dabit-industries/interbotix/interbotix-env-kinetic/blob/master/root/usr/bin/interbotix-env-init
</ol></div>

### Ubuntu Package Installation
Ubuntu apt packages are not available yet.

[dmxl]: https://gitlab.com/dabit-industries/dmxl
[package-issue]: https://gitlab.com/dabit-industries/interbotix/x-series/ros_x-series_documentation/issues/1
