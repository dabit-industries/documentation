# Turtlebot 2

<!--<center><h2>**General**</h2></center>-->
<br/>
<table class="arm-selection" style="margin: 0 auto"><tbody><tr>
<td valign="top" align="center">
[![Setup](/assets/icons/FA_tools-solid.svg){: style="height:180px;width:180px;padding:10px;"} **Setup**](setup.md)
</td>
<td valign="top" align="center">
[![Operation](/assets/icons/FA_laptop-code-solid.svg){: style="height:180px;width:180px;padding:10px;"} **Operation**](operation.md)
</td>
</tr></tbody></table>
