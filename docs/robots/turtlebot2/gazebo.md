# TurtleBot Gazebo Simulation  
This is a quick-start guide that points to the right resources to install ROS Kinetic and Gazebo 7.  

The official ROS Wiki instructions to [install ROS Kinetic on Ubuntu 16.04](http://wiki.ros.org/kinetic/Installation/Ubuntu). Install the full ROS desktop initialization.    

Gazebo 7 installation instructions can be found in this [link](http://gazebosim.org/tutorials?cat=install&tut=install_ubuntu&ver=7.0).  

Follow the instructions below to setup the packages related to Turtlebot.  

## Running the TurtleBot simulation  
You will need to have 4 terminal sessions running for this task.
Terminal 1 will run the `roscore` command to initiate the ROS framework server. 
### Terminal 1:  
```
roscore
```
  
  
Terminal 2 will use a launch file to start `Gazebo` simulation with the TurtleBot and a few objects in it.
### Terminal 2:  
```
roslaunch turtlebot_gazebo turtlebot_world.launch
```
  
![](assets/gazebo_terminal.png)  
  

![](assets/gazebo_world.png)  
  
  
Terminal 3 will use a launch file to start rviz with the robot model and other options that can be enabled / disabled by the user.
### Terminal 3:  
```
roslaunch turtlebot_rviz_launchers view_robot.launch
```
  
![](assets/tb_rviz.png)
  
    
![](assets/rviz_view.png)  
  

Terminal 4 is used to drive the TurtleBot around the world by using the commands displayed on this terminal.  
> Remember: terminal 4 must be on top of other windows while you try to control the TurtleBot.  

### Terminal 4:  
```
roslaunch turtlebot_teleop keyboard_teleop.launch
```

![](assets/teleop.png)
  
  
---
