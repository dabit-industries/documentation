# Turtlebot Setup

!!! warning "WIP"
    This page is a Work In Progress and is considered incomplete.
    A majority of this setup page will be replaced with automated scripts
    and pre-built system images (ISO/IMG).

## Overview
TurtleBot 2 is an open robotics platform designed for education and research on state of art robotics. It is also a powerful  tool to teach and learn ROS (Robot Operating System) and make the most of this cutting edge techonology. Equipped with a 3D sensor, it can map and navigate indoor enviroments. Due to the Turtlebot's modularity, you can attach your own sensors, electronics, and mechanics easily.

## Physical Assembly:

![](assets/01-explode_view_01.jpg)

## Turtlebot Ubuntu Setup

### Install Ubuntu 16.04 and ROS Kinetic using the Turtlebot 16.04 USB stick

1. Insert Turtlebot 16.04 USB stick into laptop
2. Power on laptop
2. Press `F12` to enter the [boot menu](https://support.lenovo.com/us/en/solutions/ht500222)
3. Select boot from flash drive device (`USB HDD: General UDisk`)
4. Follow the [Ubuntu Installation Guide](https://www.ubuntu.com/download/desktop/install-ubuntu-desktop)
    - Note: Check the box to allow the use of proprietary software.
5. Power off the laptop and remove the USB stick

#### Troubleshoot: No Ubuntu in boot menu
1. Power on Laptop
2. Press `F12` to enter the [boot menu](https://support.lenovo.com/us/en/solutions/ht500222)
3. Select boot from flash drive device (`USB HDD: General UDisk`)
4. Boot into the live CD by selecting `Try Ubuntu`
5. Follow the [Boot-Repair](https://help.ubuntu.com/community/Boot-Repair) instructions. 

#### Install ROS Kinetic Desktop-Full
1. [Follow the ROS Ubuntu installation guide](http://wiki.ros.org/kinetic/Installation/Ubuntu)
2. Install Turtlebot packages
  ```bash
  sudo apt install ros-kinetic-turtlebot* ros-kinetic-astra-* -y
  ```
2. Install Other required packages:
  ```bash
  sudo apt install git chrony -y
  ```

3. (Optional) Install Turtlebot Branding:
```bash
mkdir ~/tmp && cd ~/tmp
git clone https://github.com/TurtleBot-Mfg/turtlebot-doc-indigo
git clone https://github.com/TurtleBot-Mfg/turtlebot-env-indigo
git clone https://github.com/TurtleBot-Mfg/turtlebot-branding-indigo
git clone https://github.com/TurtleBot-Mfg/turtlebot-wallpapers
sudo cp -r ~/tmp/turtlebot-branding-indigo/root/lib/plymouth/themes /usr/share/plymouth/themes
sudo cp -r ~/tmp/turtlebot-branding-indigo/root/usr/share/themes /usr/share/plymouth/themes
sudo cp -r ~/tmp/turtlebot-doc-indigo/root/etc/skel/* /etc/skel/.
cp ~/tmp/turtlebot-doc-indigo/root/etc/skel/Desktop/turtlebot-doc.desktop ~/Desktop
sudo cp -r ~/tmp/turtlebot-doc-indigo/root/usr/share/doc/turtlebot /usr/share/doc/.
sudo cp -r ~/tmp/turtlebot-env-indigo/root/etc/* /etc/.
sudo cp -r ~/tmp/turtlebot-env-indigo/root/usr/share/glib-2.0/schemas /usr/share/glib-2.0/schemas/.
sudo /usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas/
sudo cp -r ~/tmp/turtlebot-wallpapers/root/usr/share/backgrounds/* /usr/share/backgrounds/.
```

4. Install [Orbbec Astra](https://github.com/orbbec/ros_astra_camera) udev rules
```bash
mkdir ~/tmp
cd ~/tmp
wget https://raw.githubusercontent.com/orbbec/astra/master/install/orbbec-usb.rules
sudo cp orbbec-usb.rules /etc/udev/rules.d/.
```

5. Setup Turtlebot Parameters in Bashrc
```bash
echo export TURTLEBOT_BASE=kobuki >> ~/.bashrc
echo export TURTLEBOT_3D_SENSOR=astra >> ~/.bashrc
echo export TURTLEBOT_STACK=hexagons >> ~/.bashrc 
```

Due to incorrect NTP time servers, configure the same NTP zone between all ROS computers:
```bash
sudo ntpdate ntp.ubuntu.com
```
You may need to install `ntpdate` first:
```bash
sudo apt-get install ntpdate -y
```

### Network Setup
Connect to a Wireless, Ethernet, or Cellular network using the Network Manager (upper right corner of desktop).
![](assets/02-wificonf.png)


Using the `hostname` command in Linux, we are able to dynamically load the IP of the computer that will be on the Turtlebot so that less configuration is needed.


1. You can find the IP of a computer by opening a terminal and typing:
```bash
hostname -I
```

2. Add the network parameters to your BashRC for the turtlebot computer  
(bashrc is a file that configures the Linux shell environment)  
    1. Open a new terminal on the turtlebot computer and type the following:  
       (Breakdown of commands explained in the next section of this document)
```bash
echo export ROS_MASTER_URI=http://\$\(hostname -I\):11311 >> ~/.bashrc
echo export ROS_IP=\$\(hostname -I\) >> ~/.bashrc
echo export ROS_HOSTNAME=\$\(hostname -I\) >> ~/.bashrc
echo export ROS_HOME=\~/.ros >> ~/.bashrc
```
(The backwards slash `\` is an escape character in a Linux terminal shell)

3. Add the network parameters to your BashRC for the master computer:  
    1. Open a new terminal on the master computer, and type the following:  
       (Replace `IP_OF_TURTLEBOT` with the ip found in step `1`)  
```bash
echo export ROS_MASTER_URI=http://IP_OF_TURTLEBOT:11311 >> ~/.bashrc
echo export ROS_IP=\$\(hostname -I\) >> ~/.bashrc
echo export ROS_HOSTNAME=\$\(hostname -I\) >> ~/.bashrc
echo export ROS_HOME=\~/.ros >> ~/.bashrc
```

4. Verify that the commands have been properly and printed to the bashrc file:
    1. Open a new terminal on both the master computer and the turtlebot computer
      * `gedit ~/.bashrc`
      * Scroll all the way down, and verify that the end of the file looks like this:
![](assets/02b-gedit_bashrc.png)

4. Load the new environment variables above into your active terminal window  
    1. On both Master and Turtlebot computers, open a terminal and type:
```bash
source ~/.bashrc
```

### Command Breakdown
```bash
echo export ROS_MASTER_URI=http://\$\(hostname -I\):11311 >> ~/.bashrc
```
  `echo` is a Linux command that prints text to a designated output (default is the terminal)  
  `export` is a Linux shell command that handles variables in the shell environment  
  `ROS_MASTER_URI` is the network address for a ROS instance to contact a ROS Master Server  
  `http://\$\(hostname -I\):11311` sets the designated network address using the current IP found by the `hostname` command, and the `\` are escape characters to prevent the hostname command from being run in the terminal  
  `>> ~/.bashrc` tells the `echo` command to send the output to the file ~/.bashrc  

```bash
echo export ROS_IP=\$\(hostname -I\) >> ~/.bashrc
```
  `echo` is a Linux command that prints text to a designated output (default is the terminal)  
 `export` is a Linux shell command that handles variables in the shell environment  
 `ROS_IP` is the network address for a ROS instance to contact a ROS Master Server  
 `\$\(hostname -I\)` sets the designated network address using the current IP found by the `hostname` command, and the `\` are escape characters to prevent the hostname command from being run in the terminal  
 `>> ~/.bashrc` tells the `echo` command to send the output to the file ~/.bashrc  

```bash
echo export ROS_HOSTNAME=\$\(hostname -I\) >> ~/.bashrc
```
 `echo` is a Linux command that prints text to a designated output (default is the terminal)  
 `export` is a Linux shell command that handles variables in the shell environment  
 `ROS_HOSTNAME` is the name of the computer on a network, used for communication on a local network  
 `\$\(hostname -I\)` sets the designated network name using the current hostname found by the `hostname` command, and the `\` are escape characters to prevent the hostname command from being run in the terminal  
 `>> ~/.bashrc` tells the `echo` command to send the output to the file ~/.bashrc  

```bash
echo export ROS_HOME=\~/.ros >> ~/.bashrc
```
 `echo` is a Linux command that prints text to a designated output (default is the terminal)  
 `export` is a Linux shell command that handles variables in the shell environment  
 `ROS_HOME` is the environment variable for the location of the ROS home folder  
 `\~/.ros` is the directory in Linux for the location of the ROS home folder  

### Network Testing
ROS requires completely free network connectivity between the Turtlebot and the Master computer.

1. Test the Ping command between the Master and the Turtlebot:
    1. On the master computer, open a new terminal:
      * `ping IP_OF_TURTLEBOT`
      * You should see the following returned if the Turtlebot is on the network:
```
PING IP_OF_TURTLEBOT (IP_OF_TURTLEBOT) 56(84) bytes of data.
64 bytes from IP_OF_TURTLEBOT: icmp_seq=1 ttl=64 time=66.8 ms
```

2. Try connecting to the ROS Master server hosted on the Turtlebot
    1. On the Turtlebot, open a new terminal and type:
      * `roscore`
    2. On the master computer, open a new terminal and type:
      * `rostopic list`
    3. If successful, you should see the following output:
```
/rosout
/rosout_agg
```

## Tips and Tricks
The following is a collection of useful tips and tricks that would help speed up aspects of connecting to and setting up the Turtlebot.

### Connecting to the Turtlebot remotely from the Master Computer
1. Open a new terminal on the Master Computer
    1. `ssh turtlebot@IP_OF_TURTLEBOT`

### Using a shell window manager
Byobu, Tmux, Screen

### Quality of Life Improvements

Stop Ubuntu from sleeping on lid close:
![](assets/01b-search_power.png)
![](assets/01b-power_prompt.png)

Stop Ubuntu from locking the screen:
![](assets/01b-search_lock.png)
![](assets/01b-lock_prompt.png)

### Additional packages

## Using Bash Aliases
[~/.bash_aliases](assets/.bash_aliases)  
[~/.rosrc](assets/.rosrc)  

at bottom of .bashrc:
```bash
if [ -f ~/.rosrc ]; then
  . ~/.rosrc
fi
```

## Troubleshooting
### Network
If your laptop cannot connect to the network, ensure that you installed [the proprietary drivers](http://askubuntu.com/questions/22118/can-i-install-extra-drivers-via-the-command-prompt).

If you continue to have issues, check out the [ROS Network Setup Page](http://wiki.ros.org/ROS/NetworkSetup)
