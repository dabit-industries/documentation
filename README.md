# Trossen Robotics Docs
This repository contains documentation for [Trossen Robotics X-Series Robot Arms](https://www.trossenrobotics.com/interbotix-x-series-robot-arms)

---

## Usage
This repo can be used either with the provided Dockerfile or using PipEnv.

The basic commands are as follow:

`Start a webserver to render and serve the files in the repository`
```bash
mkdocs serve
```

`Build static html files from the markdown`
```bash
mkdocs build
```

### Using PipEnv
```bash
pipenv shell
mkdocs
```

### Using Docker
TODO


## Contributing
Check out [contributing.md](docs/development/contributing.md)
